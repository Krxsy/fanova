import numpy as np
import visualizer
import fanova
import pyrfr.regression
from smac.configspace import ConfigurationSpace
from ConfigSpace.hyperparameters import CategoricalHyperparameter, \
    UniformFloatHyperparameter, UniformIntegerHyperparameter
import os
path = os.path.dirname(os.path.realpath(__file__))

# directory in which you can find all plots
plot_dir = path + '/example_data/test_plots'

# artificial dataset (here: features)
features = np.loadtxt(path + '/example_data/diabetes_features.csv', delimiter=",")
responses = np.loadtxt(path + '/example_data/diabetes_responses.csv', delimiter=",")

# for continuous parameters the parameter configspace consists of intervals for each variable.
pcs = list(zip( np.min(features, axis=0), np.max(features, axis=0) ))
# create ConfigSpace object and add your parameter configurations
cs = ConfigurationSpace()
for i in range(len(pcs)):
    cs.add_hyperparameter(UniformFloatHyperparameter("%i" %i, pcs[i][0], pcs[i][1]))

# create an instance of regression forest:
the_forest = pyrfr.regression.binary_rss()

# and add the forest's parameters:
the_forest.num_trees = 16
the_forest.seed=12					# reset to reseed the rng for the next fit
the_forest.do_bootstrapping=True	# default: false
the_forest.num_data_points_per_tree=0 # means same number as data points
the_forest.max_features = features.shape[1]//2 # 0 would mean all the features
the_forest.min_samples_to_split = 0	# 0 means split until pure
the_forest.min_samples_in_leaf = 0	# 0 means no restriction 
the_forest.max_depth=1024			# 0 means no restriction
the_forest.epsilon_purity = 1e-8	# when checking for purity, the data points can differ by this epsilon

# define the data and types for the random forest and train it
types = np.zeros([features.shape[1]],dtype=np.uint)
data = pyrfr.regression.numpy_data_container(features, responses, types)
the_forest.fit(data)

# create an instance of fanova with trained forest and ConfigSpace
f = fanova.fANOVA(cs, the_forest)

# marginal of particular parameter:
dims = list([1])
res = f.get_marginal(dims)
print(res)

# getting the 10 most important pairwise marginals sorted by importance
best_margs = f.get_most_important_pairwise_marginals(n=10)
print(best_margs)

# visualizations:
# first create an instance of the visualizer with fanova object and configspace
vis = visualizer.Visualizer(f, cs)
# creating the plot of pairwise marginal:
vis.plot_pairwise_marginal(list([0,2]), resolution=20)
# creating all plots in the directory
vis.create_all_plots(plot_dir)